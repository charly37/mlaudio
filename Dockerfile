FROM centos:7
MAINTAINER charles.walker.37@gmailc.om

RUN  yum -y install epel-release
RUN  yum -y install nodejs

ADD Web /var/www/Web
ADD WebDyn /var/www/WebDyn
ADD Server /var/www/Server

RUN cd /var/www/Server && npm install

EXPOSE 8080

CMD ["/usr/bin/node", "/var/www/Server/main.js"]
