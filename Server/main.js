'use strict';

const util = require('util')

//Express framework
var express = require("express");
var app     = express();

//To serve the static files
var path    = require("path");

//WebSockets
var expressWs = require('express-ws')(app);

//For session
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var expressSession = require('express-session');


//HTTPS
var url = require('url');
var fs = require('fs');
var https = require('https');

//Passport - For authentification
var passport = require('passport');
//Passport - Strategies for authentification
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
//Passport - For session
//var session = require('passport.session')
//DB
var mongoose = require('mongoose');

//my modules
var Games = require ('./Games.js');
var Game = require ('./Game.js');
var Player = require ('./Player.js');

var Users = require ('./Users.js');
//Config
var Config = require ('./Config.js');

//Express - for session
app.use(cookieParser());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(expressSession({secret:'somesecrettokenhere'}));

//Passport - For authentification
app.use(passport.initialize());
//Passport - For session
app.use(passport.session());


/*
var options = {
key: fs.readFileSync('/etc/letsencrypt/live/djynet.xyz/privkey.pem'),
cert: fs.readFileSync('/etc/letsencrypt/live/djynet.xyz/cert.pem'),
ca: fs.readFileSync('/etc/letsencrypt/live/djynet.xyz/chain.pem')
};*/

//Used to serve the static files like the images/sounds otherwise the brwoser will fails when loading images/sounds
app.use(express.static(__dirname + '/../Web'));

app.use('/node_modules', express.static(__dirname + '/node_modules'));


var aGames = new Games();
//for the user session - not link to game itself
var aUsers = new Users();



//connect to DB
var aMongoUri = process.env.MONGODB_URI ;
console.log('aMongoUri: ',aMongoUri);
mongoose.connect(aMongoUri);

var aDb = mongoose.connection;
aDb.on('error', console.error.bind(console, 'connection error:'));
aDb.once('open', function() {
  // we're connected!
  console.log('DB connection open');
});

function authenticationMiddleware () {
  console.log('authenticationMiddleware');
  return function (req, res, next) {
    //console.log('return authenticationMiddleware');
    if (req.isAuthenticated()) {
      //console.log('return next authenticationMiddleware');
      return next()
    }
    //console.log('return redirect authenticationMiddleware');
    res.redirect('/login')
  }
}

//for session serialisation in cookies
passport.serializeUser(function(user, done) {
  console.log('serializeUser');
  done(null, user._userId);
});

passport.deserializeUser(function(id, done) {
  console.log('Entering deserializeUser with id:',id);
  aUsers._userModel.findOne({ '_userId': id }, function(err, user) {
    done(err, user);
  });
});

//Google Strategy
passport.use(new GoogleStrategy({
  clientID: Config.googleAuth.clientID,
  clientSecret: Config.googleAuth.clientSecret,
  callbackURL: Config.googleAuth.callbackURL,
},
function(token, refreshToken, profile, done) {
  process.nextTick(function() {

    var aId = "GCP" + profile.id;
    console.log('Locking for google user with id: ',aId);

    aUsers._userModel.findOne({ '_userId': aId }, function(err, user) {
      if (err)
      return done(err);
      if (user) {
        console.log('User found: ',user);
        return done(null, user);
      } else {

        var aNewUser = new aUsers._userModel();
        aNewUser._googleId = profile.id;
        aNewUser._userId = "GCP" + profile.id;
        aNewUser._name = "Name";
        aNewUser._soundActivated = true;
        aNewUser._languageFrom = "French";
        aNewUser._languageTo = "French";

        console.log('Created new user: ',aNewUser);

        aNewUser.save(function(err) {
          if (err)
          throw err;
          return done(null, aNewUser);
        });
      }
    });

  });
}));
//Facebook strategy
passport.use(new FacebookStrategy({
  clientID: Config.facebookAuth.clientID,
  clientSecret: Config.facebookAuth.clientSecret,
  callbackURL: Config.facebookAuth.callbackURL,
  profileFields: ['id', 'email', 'first_name', 'last_name'],
},
function(token, refreshToken, profile, done) {
  process.nextTick(function() {
    var aId = "FB" + profile.id;
    console.log('Locking for FB user with id: ',aId);

    var aUser = aUsers.findOne(aId);
    if (aUser == null)
    {
      //console.log('No user found');
      var aNewUser = new User();
      aNewUser._facebookId = profile.id;
      aNewUser._userId = "FB" + profile.id;
      aUsers.addUser(aNewUser);
      return done(null, aNewUser);
    }
    else
    {
      //console.log('User found: ',aUser);
      return done(null, aUser);
    }
  });
}));

//Route for default index page
app.get('/',function(req,res,next)
{
  console.log('default route');
  res.sendFile(path.join(__dirname+'/../WebDyn/welcome.html'));
});

console.log('Scanning flac files');
var aSoundFiles=[];
var aSoundDirectoryPath = path.join(__dirname, '/../Web/sounds');
//passsing directoryPath and callback function
fs.readdir(aSoundDirectoryPath, function (err, files) {
  //handling error
  if (err) {
      return console.log('Unable to scan directory: ' + err);
  } 
  //listing all files using forEach
  aSoundFiles=files;
  aGames._soundFiles=aSoundFiles;
  files.forEach(function (file) {
      // Do whatever you want to do with the file
      console.log(file); 
  });
});


//Route for default index page
app.get('/Settings',authenticationMiddleware(),function(req,res,next)
{
  console.log('Settings route');
  res.sendFile(path.join(__dirname+'/../WebDyn/Settings.html'));
});

// Google routes
app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

app.get('/auth/google/callback', passport.authenticate('google', {
  successRedirect: '/',
  failureRedirect: '/login'
}));
//facebook route// Facebook routes
app.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email' }));

app.get('/auth/facebook/callback', passport.authenticate('facebook', {
  successRedirect: '/',
  failureRedirect: '/login',
}));

//Route for login page
app.get('/login',function(req,res,next)
{
  console.log('login route');
  res.sendFile(path.join(__dirname+'/../WebDyn/login.html'));
});

//Route for login page
app.get('/api/users/:user_id',authenticationMiddleware(),function(req,res,next)
{
  console.log('/api/users/:user_id with ID:', req.params.user_id);
  aUsers._userModel.findOne({ '_userId': req.params.user_id }, function(err, user) {
    if (err)
    res.send(err);
    if (user) {
      res.json(user);
    } else {
      res.send(err);
    }
  });
});



//Route for login page
app.get('/api/user',authenticationMiddleware(),function(req,res,next)
{
  //console.log('/api/users/:user_id with ID:', req.params);
  //console.log('req.isAuthenticated():', req.isAuthenticated());
  //console.log('req.user:', req.user);

  aUsers._userModel.findOne({ '_userId': req.user._userId }, function(err, user) {
    if (err)
    res.send(err);
    if (user) {
      res.json(user);
    } else {
      res.send(err);
    }
  });
});

//Route for login page
app.get('/api/challenge',authenticationMiddleware(),function(req,res,next)
{
  //console.log('/api/users/:user_id with ID:', req.params);
  //console.log('req.isAuthenticated():', req.isAuthenticated());
  //console.log('req.user:', req.user);

  var aRandChallenge = aGames._soundFiles[Math.floor(Math.random() * aGames._soundFiles.length)]
  console.log('aRandChallenge:', aRandChallenge);

  res.send("sounds/"+aRandChallenge);

  // aUsers._userModel.findOne({ '_userId': req.user._userId }, function(err, user) {
  //   if (err)
  //   res.send(err);
  //   if (user) {
  //     res.json(user);
  //   } else {
  //     res.send(err);
  //   }
  // });
  
});

//Route for login page
app.post('/api/answer',authenticationMiddleware(),function(req,res,next)
{
  //console.log('/api/users/:user_id with ID:', req.params);
  //console.log('req.isAuthenticated():', req.isAuthenticated());
  console.log('req.body:', req.body);

});

//Route for login page
app.post('/api/user',authenticationMiddleware(),function(req,res,next)
{
  //console.log('/api/users/:user_id with ID:', req.params);
  //console.log('req.isAuthenticated():', req.isAuthenticated());
  //console.log('req.user:', req.user);

  var aUpdate=
  {
    _languageFrom:req.body._languageFrom,
    _languageTo:req.body._languageTo,
    _name:req.body._name,
    _soundActivated:req.body._soundActivated
  };

  aUsers._userModel.findOneAndUpdate({ '_userId': req.user._userId }, aUpdate,  function(err, user) {
    if (err)
    res.send(err);
    if (user) {
      console.log('req.body: ',req.body);
    }
  });
});

//Route for a session (game)
app.get('/:session([a-zA-Z0-9]{6})',authenticationMiddleware(), function(req, res) {
  console.log('new game session: ',req.params, " and querry: ",req.query);

  res.sendFile(path.join(__dirname+'/../WebDyn/indexNav.html'));
});

//Route for WS
app.ws('/:session(echo[a-zA-Z0-9]{6})', function(ws, req)
{
  console.log('new ws session: ',req.params, " and querry: ",req.query, " and user: ");
console.log(ws.upgradeReq.session.passport.user);
  ws.on('message', function(msg)
  {
    //Transform the JSON in object
    var obj = JSON.parse(msg);

    //Dumping it for debug purpose
    console.log('Message Received: ',obj, " from user: ");
    //console.log('aGames: ',aGames);

    var aGameId = req.params.session;
    var aCurrGame = aGames._games.get(aGameId);
    //console.log('Looking for Game with id: ',aGameId);
    if (aGames._games.has(aGameId))
    {
      console.log('Game exists');
      aCurrGame = aGames._games.get(aGameId);
    }
    else {
      console.log('Game do not exists. Create it');
      var aNewGame = new Game();
      aNewGame._gameId=aGameId;
      aGames._games.set(aGameId,aNewGame);
      aCurrGame = aNewGame
    }

    //console.log('aGames: ',aGames);
    console.log('aCurrGame: ',aCurrGame.prettyPrint());

    if (obj.aType=="NewPlayer")
    {
      //Create a new user
      console.log('New player with ID from client :',obj.aPlayerName, ' but i use the one from session : ',ws.upgradeReq.session.passport.user);
      var aNewPlayer = new Player(obj.aPlayerName);
      aNewPlayer._playerSiteId = ws.upgradeReq.session.passport.user;
      aNewPlayer._playerName2 = obj.aPlayerName;
      //aNewPlayer._playerSettings =
      aNewPlayer._ws = ws;

      //add it to the players of this game
      //aCurrGame.addUserFromName(obj.aPlayerName);
      aCurrGame.addUserFromObj(aNewPlayer);


      console.log('Looking in mongo with id: ',aNewPlayer._playerSiteId);
      aUsers._userModel.findOne({ '_userId': aNewPlayer._playerSiteId }, function(err, user) {
        if (err)
        console.error("Error when trying to find the player info from Mongo", err)
        if (user) {
          console.log('User found in mongo: ',user);

          //send ACK
          aNewPlayer._playerSettings = user;
          console.log('aNewPlayer: ',aNewPlayer);

          var aAck = {"aType":"NewPlayerAck","aPlayerConfig":aNewPlayer};
          ws.send(JSON.stringify(aAck, aNewPlayer.replacer));

          //Send back the list of players and their status
          console.log('Sending the list of _users:',aCurrGame._users);
          var aRes = {"aType":"aUsers","aUsers":aCurrGame._users};
          aCurrGame.sendToAllPlayers(aRes);
        } else {
          console.error("No user found when trying to find the player info from Mongo")
        }
      });
    }
    else if (obj.aType=="GameStatus")
    {
      console.log('Received game status');
      aCurrGame._gameLength = obj.aGameLength;
      aCurrGame._topic = obj.aGameTopic;
      //notify all players of the new game status
      var aRes = {"aType":"aGameSt","aGame":aCurrGame}
      aCurrGame.sendToAllPlayers(aRes);

    }
  });


});

app.listen(8080);