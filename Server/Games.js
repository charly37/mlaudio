'use strict';

/** This is a class that contains all the games. */
class Games {

  /**
  * This function create a new Game.
  * @constructor
  */
  constructor()
  {
    this._games = new Map();
    this._soundFiles = [];
  }

  logMapElements(valeur, clé, map) {
    console.log("m[" + clé + "] = " + valeur);
}

  findGameForUser(aUserWs)
  {
    console.log('here: ',this._games);
    this._games.forEach(this.logMapElements);

    var aIdToDelete = undefined;

    //need to loop throw all game to find the one that has the user
    this._games.forEach(function (aGameObj,aGameId)
    {
      console.log('aPlayer: ',aGameObj);
      var aUser = aGameObj.getUserFromWs(aUserWs);
      if (aUser != null)
      {
        console.log('delete from game');
        aGameObj.deleteUser(aUser);
      }
      //if there is no more user in the game.....clea it up
      if(aGameObj.getNumberPlayers() == 0)
      {
        console.log('delete the game due to lack of users');
        //this._games.delete(aGameId);
        aIdToDelete = aGameId;
      }

    });
    if(aIdToDelete != undefined)
    {
      console.log('delete the game really due to lack of users');
      this._games.delete(aIdToDelete);
    }

    console.log('here: ',this._games);
  }

}

module.exports = Games;
