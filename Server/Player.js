'use strict';

/** This is a class that contains the player logic. */
class Player{

  /**
  * This function create a new player.
  * @constructor
  * @param {string} iName - The player name and id.
  */
  constructor(iName)
  {
    this._playerName = iName;
    this._playerName2 = "titi";
    this._playerSiteId = "";
    this._playerStatus = false;
    this._gameCreator = false;
    this._playerId = iName;
    this._ws = null;
    this._playerScore = 0;
    this._answerCurrentQuestion = false;
    this._playerSettings = {};
  }

  replacer(key,value)
  {
    if (key=="_ws") return undefined;
    else return value;
  }

  // toString override added to prototype of Foo class
toString()
{
    return "[object Foo]";
}

  prettyPrint ()
  {
    var aOutputString = "";
    aOutputString = aOutputString + "_playerName:" + this._playerName;
    aOutputString = aOutputString + " _playerStatus:" + this._playerStatus;
    aOutputString = aOutputString + " _gameCreator:" + this._gameCreator;
    aOutputString = aOutputString + " _playerId:" + this._playerId;
    aOutputString = aOutputString + " _playerScore:" + this._playerScore;
    aOutputString = aOutputString + " _answerCurrentQuestion:" + this._answerCurrentQuestion;
    return aOutputString;
  }

  /**
  * This function reset the player info.
  */
  reset()
  {
    this._answerCurrentQuestion = false;
  }
}

module.exports = Player;
