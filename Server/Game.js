'use strict';

var GameData = require ('./GameData.js');

/** This is a class that contains the game logic. */
class Game {

  /**
  * This function create a new Game.
  * @constructor
  */
  constructor()
  {
    //this.name = 'Polygon';
    this._gameStarted = false;
    this._questionCounter = 0;
    this._gameId = "";
    this._gameLength = 25;
    this._topic = "";
    this._languagelevel = 0;
    this._users = []
  }

  /**
  * This function return a randome integer between low and high.
  * @param {number} low - The low range.
  * @param {number} high - The high range.
  * @returns {Number} The random number
  */
  random (low, high)
  {
    var aRandValue = Math.floor(Math.random()*(high-low+1)+low);
    console.log('aRandValue', aRandValue);
    return aRandValue;
  }

  prettyPrint ()
  {
    var aOutputString = "";
    aOutputString = aOutputString + "_gameId:" + this._gameId;
    aOutputString = aOutputString + " _questionCounter:" + this._questionCounter;
    aOutputString = aOutputString + " _gameStarted:" + this._gameStarted;
    aOutputString = aOutputString + " _gameLength:" + this._gameLength;
    aOutputString = aOutputString + " _topic:" + this._topic;
    aOutputString = aOutputString + " _languagelevel:" + this._languagelevel;
    aOutputString = aOutputString + " _users:" + this._users;
    return aOutputString;
  }

  filtrerChallenges(aWordChallenge) {
    //if ( (aWordChallenge.usefulLevel >= this._languagelevel ) && (aWordChallenge._topic == this._topic) )
    console.log("this._topic:", this._topic, " aWordChallenge",aWordChallenge);
    if ( (true) && (aWordChallenge.tags.indexOf(this._topic) > -1) )
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  updateChallenge(iSeeds) {
    var aOneQuestion=[]
    //only kept the one with the proper level of difficulties/topic
    var arrByID = GameData.challenges.filter(this.filtrerChallenges, this);

    console.log("this._topic:", this._topic);
    //radomize the order and take the X firsts as possible translation
    var aRandPossibleChoice = arrByID.sort( function() { return 0.5 - Math.random() } );
    console.log("aRandPossibleChoice:", aRandPossibleChoice);
    aOneQuestion.push(aRandPossibleChoice[0]);
    aOneQuestion.push(aRandPossibleChoice[1]);
    aOneQuestion.push(aRandPossibleChoice[2]);
    aOneQuestion.push(aRandPossibleChoice[3]);

    console.log('aOneQuestion: ',aOneQuestion);
    //$scope._noise = $scope._challenge[Math.floor(Math.random() * $scope._challenge.length)];
    //var aPositionToInsert = Math.floor((Math.random() * 3) + 0);
    //$log.info("aPositionToInsert:", aPositionToInsert);
    //$scope._possibleTranlationRand2[aPositionToInsert]=$scope._choice;
    return aOneQuestion;
  };


  /**
  * This function return an array of seeds for the challenge choice on client side.
  * @returns {Array} Array of 4 integers that will be used as seed on all clients.
  */
  randomQuestion ()
  {
    if(true)
    {
      return this.updateChallenge();
    }
    else {
      var aSeeds = [];
      aSeeds.push(this.random(1,100));
      aSeeds.push(this.random(1,100));
      aSeeds.push(this.random(1,100));
      aSeeds.push(this.random(1,100));
      return aSeeds;
    }

  }

  /**
  * This function return a user corresponding to a WebSocket.
  * @param {Object} iWs - The WebSocket object.
  * @returns {Player} The user object
  */
  getUserFromWs(iWs)
  {
    var aFindUser = null;
    this._users.forEach(function (aUser)
    {
      console.log('Checking if this user: ',aUser, ' match the WS');
      if(aUser._ws == iWs)
      {
        aFindUser = aUser;
        console.log('User match!');
      }
    });
    return aFindUser;
  }

  /**
  * This function delete a user from the game.
  * @param {Player} iUser - The user to remove from the game.
  */
  deleteUser(iUser)
  {
    console.log('deleting user in game');
    var aUserPos = this._users.indexOf(iUser);
    if (aUserPos > -1) {
      this._users.splice(aUserPos, 1);
    }
  }

  /**
  * Return the number of player in the game
  * @returns {number} The number of player in the game
  */
  getNumberPlayers()
  {
    return this._users.length;
  }

  /**
  * This function send a payload to all the player connected to this game with WS.
  * @returns {Object} iMsg - The payload to send as an object.
  */
  sendToAllPlayers(iMsg)
  {
    console.log('Notify all player with: ',iMsg);
    var aGamers = this._users;
    this._users.forEach(function (aPlayer)
    {
      console.log('Notify a player: ');
      try {
        aPlayer._ws.send(JSON.stringify(iMsg, aPlayer.replacer));
      }
      catch(err) {
        console.log('Error: ', err.message, ' when sending WS message to ', aPlayer);
      }
    });
  }

  /**
  * This function is used to add a player to the game.
  * @param {string} iUserName - The player name.
  */
  addUserFromName(iUserName)
  {
    console.log('Adding new user in the game engine');
    var aNewPlayerObj = new Player(iUserName);
    this._users.push(aNewPlayerObj);
  }

  /**
  * This function is used to add a player to the game.
  * @param {Player} iPlayer - The player object.
  */
  addUserFromObj(iPlayer)
  {
    console.log('Adding new user in the game engine');
    this._users.push(iPlayer);
  }

  /**
  * This is use to know if all players are ready.
  * @returns {Boolean}
  */
  allPlayerAreReady()
  {
    var aAllPayerReady = true;
    console.log("this",this);
    this._users.forEach(function(value)
    {
      console.log("Checking if player: ",value, " is ready");
      if (value._playerStatus == false)
      {
        console.log("Player is not ready");
        aAllPayerReady = false;

      }
    });
    return aAllPayerReady;
  }

  /**
  * This is use to reset the players info that depend on the current question. This is called when a new question is send to the players
  */
  updateNewQuestion()
  {
    this._users.forEach(function(aPlayer)
    {
      console.log("Reseting player: ",aPlayer);
      aPlayer.reset();
    });
  }

  /**
  * This is use to know if all players answer the current question.
  * @returns {Boolean} The boolean which is true if all player answer the current question. False otherwise.
  */
  allPlayerHaveAnswer()
  {
    var aAllPayerAnswer = true;
    this._users.forEach(function(aPlayer)
    {
      console.log("Checking if player: ",aPlayer, " is ready");
      if (aPlayer._answerCurrentQuestion == false)
      {
        console.log("Player has not answer");
        aAllPayerAnswer = false;
      }
    });
    return aAllPayerAnswer;
  }

  /** This is use to update the status of a player. */
  /**
  * This is use to update the status of a player.
  * @param {string} iPlayerId - The player id.
  * @param {Object} iPlayer - The player.
  */
  updatePlayerStatus(iPlayerId, iPlayer)
  {
    this._users.forEach(function(aPlayer)
    {
      console.log("Checking player: ",aPlayer);
      if (aPlayer._playerName == iPlayerId)
      {
        console.log("Player found");
        aPlayer._playerStatus = iPlayer.aPlayerStatus;
      }
    });
  }

  /** This is use to update the status of a player. */
  /**
  * This is use to update the status of a player.
  * @param {string} iPlayerId - The player id.
  * @param {Object} iPlayer - The player.
  */
  updatePlayerScore(iPlayerId, iPlayer)
  {
    console.log("Updating score for player with ID: ",iPlayerId);
    this._users.forEach(function(aPlayer)
    {
      if (aPlayer._playerId == iPlayerId)
      {
        console.log("Player found");
        if(iPlayer.aPlayerAnswerStatus){
          aPlayer._playerScore = aPlayer._playerScore+1;
        }
        aPlayer._answerCurrentQuestion=true;
      }
    });
    //Now do something if all player aPlayerAnswerStatus
  }

}

module.exports = Game;
