'use strict';
var mongoose = require('mongoose');
/** This class represent the webiste visitors. */
class Users {



  /**
  * This function create a new visitors handler.
  * @constructor
  */
  constructor()
  {
    this._userSchema = mongoose.Schema({
      _userId: String,
      _googleId: String,
      _facebookId: String,
      _name: String,
      _languageFrom: String,
      _languageTo: String,
      _soundActivated: Boolean,
      _initSetupDone: Boolean
    });
    this._userModel = mongoose.model('User', this._userSchema);
  }

}

module.exports = Users;
