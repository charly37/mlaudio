/**
 * @ngdoc controller
 * @name RgridApplication.controller:welcomeCtrl
 * @description
 * This controler is reponsible of the welcome page
 */
RgridApplication.controller('welcomeCtrl', function($scope, $log) {

  /////////////
  //functions//
  /////////////

  $scope.makeid = function ()
  {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 6; i++ )
    text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  $scope.startNewSingleSession = function(){
    aSession = $scope.makeid();
    $log.info("Starting a new single session:", aSession);
    return aSession;
  };

  $scope.sessionLink = $scope.startNewSingleSession();
  $log.info("$scope.sessionLink:", $scope.sessionLink);

  /////////////////
  //END functions//
  /////////////////

});
