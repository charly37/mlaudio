/**
* @ngdoc controller
* @name RgridApplication.controller:Providers_Tab
* @description
* This controler is responsible for the setting update
*/
RgridApplication.controller('Settings', function($scope, $uibModal, DataShared, DataProvider, $log, $http) {

  /////////////
  //variables//
  /////////////

  //object retrieve from other service
  $scope._languages = DataProvider._languages;

  //$scope._user = None;
  $scope._user = {};

  //Used for the dropdown menu
  $scope.aDropDownFromLanguageStatus = {isopen: false};
  $scope.aDropDownToLanguageStatus = {isopen: false};
  $scope.aDropDownLanguageDeepth = {isopen: false};

  /////////////
  //functions//
  /////////////

  //function called when the user select a new value in one of the 2 dropdown
  $scope.setChoice = function(iSelectedBox,iData){
    if (iSelectedBox == "FROM")
    {
      $log.info("Language FROM selected: ",iData);
      $scope._user._languageFrom = iData;
    }
    else
    {
      $log.info("Language TO selected: ",iData);
      $scope._user._languageTo = iData;
    }
  };

  //function called when the user click the "save" button


  $scope.save = function(){
    $log.info("Saving the config");
    $http.post('/api/user', JSON.stringify($scope._user)).
    success(function(data, status, headers, config) {
        // this callback will be called asynchronously
        // when the response is available
        console.log(data);
      }).
      error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });

  };

  //call the server to retrieve user DataShared
  $http.get('/api/user').
  then(function(response) {
    console.log('response.data: ',response);
    $scope._user._name=response.data._name;
    $scope._user._languageFrom = response.data._languageFrom;
    $scope._user._languageTo = response.data._languageTo;
    $scope._user._soundActivated = response.data._soundActivated;
  });

});
