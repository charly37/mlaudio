/**
 * @ngdoc controller
 * @name RgridApplication.controller:Providers_Templates
 * @description
 * This controler is responsible for the game update
 */
RgridApplication.controller('Providers_Templates', function ($scope, $uibModal, $http, DataShared, DataProvider, $log) {

  $log.debug("Requesting a new challenge");
  $http.get("/api/challenge", {})
    .success(function (data, status, headers, config) {
      $log.debug("New challenge is: ", data);
      $scope._soundFile = data;
    }).error(function (data, status, headers, config) {
      $log.error("Error when requesting a new challenge");
    });

  /////////////
  //functions//
  /////////////

  /**
   * @ngdoc method
   * @name SayWord
   * @methodOf RgridApplication.controller
   * @description
   * Play a sound corresponding to the work choosen by the user
   */
  $scope.SayWord = function () {
    $log.info("Playing the sound");
    var aAudioStream = new Audio("sounds/example.flac");
    aAudioStream.controls = true;
    aAudioStream.play();
  };

  /**
   * @ngdoc method
   * @name submitChallenge
   * @methodOf RgridApplication.controller
   * @description
   * This methode send the response of the user to a challenge
   */
  //function called when the user click the "submit" button
  $scope.submitChallenge = function () {
    var aAnswer={"_soundFile":$scope._soundFile,"_wordIsPresent":$scope._wordIsPresent,"_wordTiming":$scope._wordTiming}
    $log.info("Challenge submitted. aAnswer? ", aAnswer);
    //DataShared._updateUserServer($scope._user._playerName, $scope._readyToPlay);

    $http.post('/api/answer', JSON.stringify(aAnswer)).
      success(function (data, status, headers, config) {
        // this callback will be called asynchronously
        // when the response is available
        console.log(data);
      }).
      error(function (data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });


  };

  /////////////
  //variables//
  /////////////
  $scope._soundFile = "sounds/example.flac";
  $scope._wordIsPresent = false;
  $scope._wordTiming = "";
});

