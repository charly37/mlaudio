/**
 * @ngdoc factory
 * @name RgridApplication.factory:DataProvider
 * @description
 * This factory is reponsible to provide data about the game content
 */
RgridApplication.factory('DataProvider', function($log){
  //Provide content

  //The object that contains the config and will be shared
  var _dataProvider = {};
  _dataProvider._challenges=[
    {image:"images/pug-690566_640.jpg", sounds:{Arabic:"sounds/2016_09_18_21_01_26.mp3", French:"sounds/chien.mp3"}, English:"Dog", French:"Chien", Arabic:"Kalb", originalImageUrl:"https://pixabay.com/en/pug-puppy-dog-animal-cute-690566/", tags:["animals"], usefulLevel:2},
    {image:"images/armchair-1498666_640.png", sounds:{Arabic:"sounds/2016_09_22_20_48_41.mp3", French:"sounds/2016_09_18_21_00_34.mp3"}, English:"chair", French:"chaise", Arabic:"kursi", originalImageUrl:"https://pixabay.com/en/armchair-chair-furniture-seat-1498666/", tags:["house"], usefulLevel:2},
    {image:"images/window-941625_640.jpg", sounds:{Arabic:"sounds/2016_09_22_20_49_08.mp3", French:"sounds/2016_09_18_21_00_36.mp3"}, English:"window", French:"fenêtre", Arabic:"naafeda", originalImageUrl:"https://pixabay.com/en/window-prague-twins-941625/", tags:["house"], usefulLevel:2}
  ];
  _dataProvider._languages = ["French","Arabic","English"];
  _dataProvider._topics = ["ff"];

  return _dataProvider;
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//factory vs service : http://blog.thoughtram.io/angular/2015/07/07/service-vs-factory-once-and-for-all.html

/**
 * @ngdoc factory
 * @name RgridApplication.factory:DataShared
 * @description
 * This factory is reponsible to share data about the game settings and communicate with the server using websockets
 */
RgridApplication.factory('DataShared', function($log, $rootScope){
  //Provide settings

  //The object that contains the config and will be shared
  var _dataHolder = {};

  _dataHolder._languagelevel = 0;
  _dataHolder._gameLength = 0;
  _dataHolder._topic = "";
  _dataHolder._users = [];
  _dataHolder._seeds = [];
  _dataHolder._userId = "0";
  _dataHolder._userName = "";
  _dataHolder._languagelevel = 2;
  _dataHolder._gameLength = 25;
  _dataHolder._topic = "";
  _dataHolder._gameURL = "";
  _dataHolder._users = [];
  _dataHolder._seeds = [1,2,3,4];
  _dataHolder._userName ="";

  _dataHolder.makeid = function ()
  {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
    text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }
  //_dataHolder._userName = _dataHolder.makeid();

  //for the watch from controler setting
  _dataHolder._getUsers = function ()
  {
    $log.info("Watch callback in service DataShared. _dataHolder._users: ",_dataHolder._users)
    return _dataHolder._users;
  };

  //for the watch from controler setting
  _dataHolder._getSeeds = function ()
  {
    $log.info("Watch callback in service DataShared. _dataHolder._seeds: ",_dataHolder._seeds)
    return _dataHolder._seeds;
  };

  return _dataHolder;
});

//Declare the Tab controler
RgridApplication.controller('TabsDemoCtrl', function($scope, DataShared,$log) {
  //Define the type of tabs : https://angular-ui.github.io/bootstrap/#/tabs
  $scope.navType = 'pills';


});
