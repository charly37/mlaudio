// Your app's root module...
var RgridApplication = angular.module('Rgrid', ['ngAnimate', 'ui.bootstrap'], function($httpProvider) {
  $httpProvider.defaults.useXDomain = true;
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
});

RgridApplication.config(function($sceDelegateProvider) {
  $sceDelegateProvider.resourceUrlWhitelist([
    "self",
    /sounds/,
  ]);
})