# Introduction
Application to create ML training set.

## Build

### Native
You can either use directly nodejs
Use NPM to install the deps with :
npm install #(it will use the package.json which contains the deps)

### Docker
You can also use Docker to build/run
sudo docker build .

## Run

### Native
Start it with :
sudo node -verbose main.js

env var :
- GoogleId
- GoogleSecret
- FacebookId
- FacebookSecret
- MONGODB_PORT_27017_TCP_ADDR

### Docker
You can also use Docker to build/run
sudo docker run --name mongoDb -d -v /mongoData:/data/db mongo

sudo docker run -p 80:8080 -e "GoogleId=XXXXXX.apps.googleusercontent.com" -e "GoogleSecret=XXXXXX" -e "FacebookId=123456789" -e "FacebookSecret=XXXXXX" --name language --link mongoDb:mongoDb -d charly37/language

docker rm -v $(docker ps --filter status=exited -q) ; docker volume rm $(docker volume ls -q -f 'dangling=true') ; docker rmi $(sudo docker images -f "dangling=true" -q)


## documentation

### server
The server use JSDOC to generate its documentation
http://usejsdoc.org/
jsdoc main.js -d Documentation/Server

### Client
The client use ngdoc to generate its documentation
